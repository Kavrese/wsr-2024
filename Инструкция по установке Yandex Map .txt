Инструкция по установке Yandex Map Kit:
1) В pubspec.yaml, в теге dependencies, добавте yandex_mapkit: ^3.4.0
2) pub get
3) Перейдите в папку \android\app\src\main\kotlin\com\example\{название проекта} и создайте новый Java Class "MainApplication" (правая кнопка мыши по папке, New -> Java Class)
4) В этом файле должно быть следующие содержимое
```
package com.example.{название проекта};

import android.app.Application;

import com.yandex.mapkit.MapKitFactory;

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MapKitFactory.setApiKey("API KEY");
    }
}
```
5) Перейдите в папку final_wsr_2023\android\app\src\main, откройте файл AndroidManifest.xml и допишите его до такого содержания:
```
<manifest xmlns:android="http://schemas.android.com/apk/res/android">

    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />

    <application
        android:label="final_wsr_2023"
        android:name=".MainApplication"
        android:icon="@mipmap/ic_launcher">
        <activity
            android:name=".MainActivity"
            android:exported="true"
            android:launchMode="singleTop"
            android:theme="@style/LaunchTheme"
            android:configChanges="orientation|keyboardHidden|keyboard|screenSize|smallestScreenSize|locale|layoutDirection|fontScale|screenLayout|density|uiMode"
            android:hardwareAccelerated="true"
            android:windowSoftInputMode="adjustResize">
            <!-- Specifies an Android theme to apply to this Activity as soon as
                 the Android process has started. This theme is visible to the user
                 while the Flutter UI initializes. After that, this theme continues
                 to determine the Window background behind the Flutter UI. -->
            <meta-data
              android:name="io.flutter.embedding.android.NormalTheme"
              android:resource="@style/NormalTheme"
              />
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.LAUNCHER"/>
            </intent-filter>
        </activity>
        <!-- Don't delete the meta-data below.
             This is used by the Flutter tool to generate GeneratedPluginRegistrant.java -->
        <meta-data
            android:name="flutterEmbedding"
            android:value="2" />
    </application>
</manifest>
```
Т.е. добавте строчки:
```
    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```
и измените android:name в теге application на ".MainApplication"
7) Перейдите в папку android\app и откройте файл build.gradle. В теге defaultConfig, который является подтегом android, замените minSdkVersion flutter.minSdkVersion на 21
8) В том же файле найдеит тег dependencies (обычно он в самом конце), и в его тело впишите implementation 'com.yandex.android:maps.mobile:4.4.0-full'. Должно получиться как-то так:
```
dependencies {
    implementation 'com.yandex.android:maps.mobile:4.4.0-full'
}
```
