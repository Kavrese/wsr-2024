import 'package:final_wsr_2023/auth/domain/utils.dart';
import 'package:final_wsr_2023/common/app.dart';
import 'package:final_wsr_2023/on_boarding/data/repository/on_boarding_repository.dart';
import 'package:final_wsr_2023/on_boarding/domain/queue.dart';
import 'package:final_wsr_2023/on_boarding/presentation/pages/page_on_boarding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';



void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  Queue queue = Queue();

  group("Тестирование экрана Onboarding", () {

    test("Изображения и текста из очереди извлекаются правильно", () {
      queue.resetQueue();
      var originData = getOnBoardingItems();
      for (int i=0; i < originData.length; i++){
        var current = queue.next();
        expect(originData[i], current);
      }
    });

    test("Корректное извлечение из очереди", () {
      queue.resetQueue();
      var originLength = queue.length();
      queue.next();
      expect(queue.length(), originLength-1);
    });

    testWidgets("В случае когда в очереди несколько картинок, устанавливается правильная надпись на кнопке", (tester) async {
      queue.resetQueue();

      final pageView = find.byType(PageView);

      await tester.runAsync(() => tester.pumpWidget(const MaterialApp(
            home: OnBoardingPage(),
      )));

      var elem = queue.next();
      await tester.pumpAndSettle();
      expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);

      final titleText = find.text(elem["title"]!);
      await tester.dragUntilVisible(titleText, pageView, const Offset(-250, 0));

      elem = queue.next();
      await tester.pumpAndSettle();
      expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);
    });
    
    testWidgets("Случай, когда очередь пустая, надпись на кнопке должна измениться на \"Sing Up\".", (tester) async {
      queue.resetQueue();

      final pageView = find.byType(PageView);

      await tester.runAsync(() => tester.pumpWidget(
          const MaterialApp(
            home: Scaffold(
              body: OnBoardingPage(),
            ),
          )
      )
      );

      var elem = queue.next();

      await tester.pumpAndSettle();
      await tester.dragUntilVisible(
          find.text(elem["title"]!), pageView, const Offset(-250, 0));
      await tester.pumpAndSettle();

      elem = queue.next();
      await tester.dragUntilVisible(
          find.text(elem["title"]!), pageView, const Offset(-250, 0));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(FilledButton, "Sign Up"), findsOneWidget);
    });

    test("Проверка валидация почты", (){
      expect(true, isCorrectEmail("test@gmail.com"));
      expect(false, isCorrectEmail("AD@gmail.com"));
      expect(true, isCorrectEmail("teAst@gmail.com"));
      expect(false, isCorrectEmail("teast@Gmail.com"));
    });
  });
}