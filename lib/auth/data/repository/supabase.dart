import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:final_wsr_2023/auth/domain/utils.dart';
import 'package:final_wsr_2023/common/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


final supabase = Supabase.instance.client;

Future<void> signUp(
  {
    required String fullname,
    required String phone,
    required String email,
    required String password,
    required String confirmPassword,
    required Function(User user) onResponse,
    required Function(String) onError
  }
) async {
  var existNetwork = await checkNetworkConnection();
  if (!existNetwork){
    onError("Network error");
    return;
  }
  try {
    if (!isCorrectEmail(email)){
      onError("Почта не корректна");
      return;
    }
    if (password != confirmPassword) {
      onError("Пароли не совпадают");
      return;
    }
    var hashPassword = sha256.convert(utf8.encode(password)).toString();
    var result = await supabase.auth.signUp(
      password: hashPassword,
      email: email,
    );
    if (result.user == null) {
      onError("User is null");
    } else {
      await fillRegisterUser(fullname, phone);
      onResponse(result.user!);
    }
  } on AuthException catch (e) {
    onError(e.message);
  }
}

Future<void> signIn(
  {
    required String email,
    required String hashPassword,
    required Function(User) onResponse,
    required Function onError
  }
) async {
  try {
    var existNetwork = await checkNetworkConnection();
    if (!existNetwork){
      onError("Network error");
      return;
    }
    if (!isCorrectEmail(email)){
      onError("Почта не корректна");
      return;
    }
    var result = await supabase.auth.signInWithPassword(
      password: hashPassword,
      email: email
    );
    if (result.user == null) {
      onError("User is null");
    } else {
      onResponse(result.user!);
    }
  } on AuthException catch (e) {
    onError(e.message);
  }
}

Future<void> fillRegisterUser(String fullname, String phone) async {
  await supabase.from("profiles").insert({
    "fullname": fullname,
    "id_user": supabase.auth.currentUser!.id,
    "avatar": "",
    "phone": phone
  });
}

Future<List<Map<String, dynamic>>> updateStatusOrder(
    int newStatus, String id) async {
  return await supabase
      .from("orders")
      .update({"status": newStatus})
      .eq("id", id)
      .select();
}

Future<List<Map<String, dynamic>>> getAllOrders() async {
  // Получение списка заказов текущего пользователя
  return await supabase
      .from("orders")
      .select()
      .eq("id_user", supabase.auth.currentUser!.id);
}

Future<List<Map<String, dynamic>>> fetchAds() async {
  //Получение списка рекламы
  var res = await supabase.from("ads").select("ads_url");
  return res;
}

Future<void> sendEmailCode(
    {required String email,
    required Function onResponse,
    required Function(String) onError}) async {
  try {
    await supabase.auth.resetPasswordForEmail(email);
    onResponse();
  } on AuthException catch (e) {
    onError(e.message);
  } on Exception catch (e) {
    onError("Something went wrong");
  }
}

Future<void> changePassword(
    String newPassword,
    {
      required Function onResponse,
      required Function(String) onError
    }
) async {
  try {
    await supabase.auth.updateUser(
        UserAttributes(
          password: newPassword,
        )
    );
    onResponse();
  } on AuthException catch (e) {
    onError(e.message);
  }
}

Future<void> logout() async {
  await supabase.auth.signOut();
}

Future<void> verifyOTP(String email, String code, {
  required Function onResponse,
  required Function(String) onError
}) async {
  try {
    await supabase.auth.verifyOTP(
        type: OtpType.email,
        token: code,
        email: email
    );
    onResponse();
  } on AuthException catch (e) {
    onError(e.message);
  } on Exception catch (e) {
    onError("Something went wrong");
  }
}

Future<List<Map<String, dynamic>>> getPoints() async {
  return await supabase
      .from("points")
      .select();
}

Future<void> savePassword(String password) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var bytes = utf8.encode(password);
  var shaPassword = sha256.convert(bytes).toString();
  sharedPreferences.setString("hash_password", shaPassword);
}
