import 'dart:convert';

import 'package:crypto/crypto.dart';

bool isCorrectEmail(String email){
  return RegExp(r'[a-z0-9]+@[a-z0-9]+\.\w+').hasMatch(email);
}

String hashPassword(String password){
  return sha256.convert(utf8.encode(password)).toString();
}