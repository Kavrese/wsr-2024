import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:final_wsr_2023/auth/data/repository/supabase.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_sign_in.dart';
import 'package:final_wsr_2023/auth/presentation/widgets/CustomTextField.dart';
import 'package:final_wsr_2023/common/app.dart';
import 'package:final_wsr_2023/common/widgets/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var fullName = TextEditingController();
  var email = TextEditingController();
  var password = TextEditingController();
  var phoneNumber = MaskedTextController(mask: "+0(000)000-00-00");
  var confirmPassword = TextEditingController();

  var obscurePassword = true;
  var obscureConfirmPassword = true;

  var policy = false;
  var enableButton = false;


  void isValid() {
    setState(() {
      enableButton = fullName.text.isNotEmpty &&
          phoneNumber.text.isNotEmpty &&
          email.text.isNotEmpty &&
          password.text.isNotEmpty &&
          confirmPassword.text.isNotEmpty &&
          policy &&
          phoneNumber.text.length == 16;
    });
  }

  Future<void> savePassword(String password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var bytes = utf8.encode(password);
    var shaPassword = sha256.convert(bytes).toString();
    sharedPreferences.setString("hash_password", shaPassword);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 78),
              Text(
                "Create an account",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 8),
              Text(
                "Complete the sign up process to get started",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              const SizedBox(height: 33),
              CustomTextField(
                label: "Full name",
                hint: "Ivanov Ivan",
                controller: fullName,
                onChange: (newText) {
                  isValid();
                },
              ),
              const SizedBox(height: 24),
              CustomTextField(
                label: "Phone Number",
                hint: "+7(999)999-99-99",
                controller: phoneNumber,
                onChange: (newText) {
                  if (newText == "+") {
                    setState(() {
                      phoneNumber.clear();
                    });
                  }
                  isValid();
                },
              ),
              const SizedBox(height: 24),
              CustomTextField(
                  label: "Email Address",
                  hint: "***********@mail.com",
                  onChange: (newText) {
                    isValid();
                  },
                  controller: email),
              const SizedBox(height: 24),
              CustomTextField(
                label: "Password",
                hint: "***********",
                controller: password,
                isObscure: obscurePassword,
                onChange: (newText) {
                  isValid();
                },
                onTapSuffix: () {
                  setState(() {
                    obscurePassword = !obscurePassword;
                  });
                },
              ),
              const SizedBox(height: 24),
              CustomTextField(
                label: "Confirm Password",
                hint: "***********",
                controller: confirmPassword,
                isObscure: obscureConfirmPassword,
                onChange: (newText) {
                  isValid();
                },
                onTapSuffix: () {
                  setState(() {
                    obscureConfirmPassword = !obscureConfirmPassword;
                  });
                },
              ),
              const SizedBox(height: 37),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 18,
                    height: 18,
                    child: Checkbox(
                        value: policy,
                        onChanged: (newValue) {
                          setState(() {
                            policy = newValue!;
                          });
                          isValid();
                        }),
                  ),
                  const SizedBox(width: 11),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 69),
                      child: GestureDetector(
                        onTap: () async {
                          await launchUrl(Uri.parse('https://cfjtdxsfxtqxnymxrowo.supabase.co/storage/v1/object/public/files/politic.pdf'));
                        },
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: const TextSpan(children: [
                            TextSpan(
                                text: "By ticking this box, you agree to our",
                                style: TextStyle(color: Color(0xFFA7A7A7))),
                            TextSpan(
                                text: " Terms and conditions and private policy",
                                style: TextStyle(color: Color(0xFFEBBC2E))),
                          ], style: TextStyle(fontSize: 12)),
                        ),
                      ),
                    )
                  )
                ],
              ),
              const SizedBox(height: 64),
              SizedBox(
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (enableButton)
                        ? () async {
                            showLoading(context);
                            await signUp(
                                fullname: fullName.text,
                                phone: phoneNumber.text,
                                email: email.text,
                                password: password.text,
                                confirmPassword: confirmPassword.text,
                                onResponse: (User user) async {
                                  await savePassword(password.text);
                                  setState(() {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => const SignInPage()
                                        ),
                                        (Route<dynamic> route) => false
                                    );
                                  });
                                },
                                onError: (String error) {
                                  Navigator.pop(context);
                                  showErrorDialog(context, error);
                                });
                          } : null,
                    child: Text("Sign Up",
                        style: Theme.of(context).textTheme.labelLarge),
                  )),
              const SizedBox(height: 20),
              SizedBox(
                width: double.infinity,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SignInPage()
                        )
                    );
                  },
                  child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        TextSpan(
                            text: "Already have an account?",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(fontWeight: FontWeight.normal)),
                        TextSpan(
                            text: "Sign in",
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500)),
                      ])),
                ),
              ),
              const SizedBox(height: 18),
              SizedBox(
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("or sign in usign", style: Theme.of(context)
                        .textTheme
                        .titleMedium),
                    const SizedBox(height: 8),
                    Image.asset("assets/google.png"),
                    const SizedBox(height: 28),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
