import 'package:final_wsr_2023/auth/data/repository/supabase.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_sign_up.dart';
import 'package:final_wsr_2023/common/widgets/dialogs.dart';
import 'package:flutter/material.dart';

class SetNewPasswordPage extends StatefulWidget {
  const SetNewPasswordPage({super.key});

  @override
  State<SetNewPasswordPage> createState() => _SetNewPasswordPageState();
}

class _SetNewPasswordPageState extends State<SetNewPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FilledButton(
          onPressed: () async {
            showLoading(context);
            await changePassword("123456", onResponse: () async {
              await logout();
              setState(() {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (_) => const SignUpPage()),
                        (route) => false);
              });
            }, onError: (error) {
              Navigator.pop(context);
              showErrorDialog(context, error);
            });
          },
          child: const Text("CHANGE PASSWORD"),
        ),
      ),
    );
  }
}
