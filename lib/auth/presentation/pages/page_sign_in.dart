import 'package:email_validator/email_validator.dart';
import 'package:final_wsr_2023/auth/data/repository/supabase.dart';
import 'package:final_wsr_2023/auth/domain/utils.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_forgot_password.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_sign_up.dart';
import 'package:final_wsr_2023/common/widgets/dialogs.dart';
import 'package:final_wsr_2023/home/presentation/pages/page_home.dart';
import 'package:flutter/material.dart';
import 'package:final_wsr_2023/auth/presentation/widgets/CustomTextField.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {

  late SharedPreferences sharedPreferences;

  var email = TextEditingController();
  var password = TextEditingController();

  var obscurePassword = true;

  var rememberMe = false;
  var enableButton = false;

  void isValid() {
    setState(() {
      enableButton =
          email.text.isNotEmpty &&
          password.text.isNotEmpty;
    });
  }

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var email = sharedPreferences.getString("email");
    if (email == null){
      return;
    }
    var hashPassword = sharedPreferences.getString("password")!;
    await login(email, hashPassword, false);
  }

  Future<void> login(String email, String password, bool trySave) async {
    if (!EmailValidator.validate(email)) {
      showErrorDialog(context, "Почта не корректна");
      return;
    }
    showLoading(context);
    await signIn(
      email: email,
      hashPassword: password,
      onResponse: (User user) async {
        if (trySave) {
          await sharedPreferences.setString("email", email);
          await sharedPreferences.setString("password", hashPassword(password));
          await sharedPreferences.setBool("isRememberMe", rememberMe);
        }
        setState(() {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => const HomePage()
              ),
                  (Route<dynamic> route) => false
          );
        });
      },
      onError: (String error) {
        Navigator.pop(context);
        showErrorDialog(context, error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 155),
              Text(
                "Welcome Back",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 8),
              Text(
                "Fill in your email and password to continue",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              const SizedBox(height: 24),
              CustomTextField(
                  label: "Email Address",
                  hint: "***********@mail.com",
                  onChange: (newText) {
                    isValid();
                  },
                  controller: email),
              const SizedBox(height: 24),
              CustomTextField(
                label: "Password",
                hint: "***********",
                controller: password,
                isObscure: obscurePassword,
                onChange: (newText) {
                  isValid();
                },
                onTapSuffix: () {
                  setState(() {
                    obscurePassword = !obscurePassword;
                  });
                },
              ),
              const SizedBox(height: 17),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 18,
                    height: 18,
                    child: Checkbox(
                        value: rememberMe,
                        onChanged: (newValue) {
                          setState(() {
                            rememberMe = newValue!;
                          });
                        },
                        side: const BorderSide(
                          color: Color(0xFFA7A7A7),
                        ),
                    ),
                  ),
                  const SizedBox(width: 4),
                  Expanded(
                    child: Text("Remember password", style: Theme.of(context)
                        .textTheme
                        .titleMedium!,),
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (_) => const ForgotPasswordPage()));
                    },
                    child: Text("Forgot password?", style: Theme.of(context).textTheme.labelSmall!,)
                  )
                ],
              ),
              const SizedBox(height: 187),
              SizedBox(
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (enableButton) ? () async {
                      await login(
                        email.text,
                        hashPassword(password.text),
                        true
                      );
                    } : null,
                    child: const Text("Log in"),
                  )),
              const SizedBox(height: 20),
              SizedBox(
                width: double.infinity,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SignUpPage()
                        )
                    );
                  },
                  child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        TextSpan(
                            text: "Already have an account?",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(fontWeight: FontWeight.normal)),
                        TextSpan(
                            text: "Sign up",
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w500)),
                      ])),
                ),
              ),
              const SizedBox(height: 18),
              SizedBox(
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("or log in usign", style: Theme.of(context)
                        .textTheme
                        .titleMedium),
                    const SizedBox(height: 8),
                    Image.asset("assets/google.png"),
                    const SizedBox(height: 28),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
