import 'package:email_validator/email_validator.dart';
import 'package:final_wsr_2023/auth/data/repository/supabase.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_otp.dart';
import 'package:final_wsr_2023/auth/presentation/widgets/CustomTextField.dart';
import 'package:flutter/material.dart';

import '../../../common/widgets/dialogs.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  var enableButton = false;
  var email = TextEditingController();

  void isValid() {
    setState(() {
      enableButton = email.text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 155),
            Text(
              "Forgot Password",
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 8),
            Text(
              "Enter your email address",
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(height: 56),
            CustomTextField(
                label: "Email Address",
                hint: "***********@mail.com",
                onChange: (newText) {
                  isValid();
                },
                controller: email),
            const SizedBox(height: 56),
            SizedBox(
                width: double.infinity,
                child: FilledButton(
                  onPressed: (enableButton)
                      ? () async {
                          if (!EmailValidator.validate(email.text)) {
                            showErrorDialog(context, "Почта не корректна");
                            return;
                          }
                          showLoading(context);
                          await sendEmailCode(
                              email: email.text,
                              onResponse: () {
                                Navigator.pop(context);
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => OTPPage(email: email.text)));
                              },
                              onError: (String error) {
                                Navigator.pop(context);
                                showErrorDialog(context, error);
                              });
                        }
                      : null,
                  child: Text("Send OTP",
                      style: Theme.of(context).textTheme.labelLarge),
                )),
            const SizedBox(height: 20),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: SizedBox(
                width: double.infinity,
                child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: [
                      TextSpan(
                          text: "Remember password? Back to",
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(fontWeight: FontWeight.normal)),
                      TextSpan(
                          text: "Sign in",
                          style: Theme.of(context).textTheme.labelSmall),
                    ])),
              ),
            )
          ],
        ),
      ),
    );
  }
}
