import 'package:final_wsr_2023/auth/data/repository/supabase.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_set_new_password.dart';
import 'package:final_wsr_2023/common/widgets/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:timer_count_down/timer_count_down.dart';

class OTPPage extends StatefulWidget{

  final String email;

  const OTPPage({super.key, required this.email});

  @override
  State<OTPPage> createState() => _OTPPageState();
}



class _OTPPageState extends State<OTPPage> {

  var enableButton = false;
  var lostSecond = 60;
  var isError = false;
  TextEditingController code = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    double sizeSeparator = widthScreen / 6 - 32;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 155),
            Text(
              "OTP Verification",
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 8),
            Text(
              "Enter the 6 digit numbers sent to your email",
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(height: 52),
            Pinput(
              length: 6,
              controller: code,
              separatorBuilder: (context) => SizedBox(width: sizeSeparator),
              defaultPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFFA7A7A7)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              focusedPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFF0560FA)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              submittedPinTheme: (!isError) ? PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFF0560FA)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ) : PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFFED3A3A)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              onChanged: (text){
                setState(() {
                  isError = false;
                  enableButton = text.length == 6;
                });
              },
            ),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("If you didn’t receive code, "),
                (lostSecond != 0) ? Countdown(
                  seconds: 60,
                  build: (BuildContext context, double time) {
                    lostSecond = time.toInt();
                    var lostMin = lostSecond ~/ 60;
                    var lostDiffSeconds = lostSecond - lostMin * 60;
                    return Text("$lostMin:${(lostDiffSeconds < 10) ? "0$lostDiffSeconds" : lostDiffSeconds}");
                  },
                  interval: const Duration(seconds: 1),
                  onFinished: () {
                    setState(() {});
                  },
                ) : GestureDetector(onTap: (){
                  showLoading(context);
                  sendEmailCode(email: widget.email, onResponse: (){
                    setState(() {
                      lostSecond = 60;
                    });
                    Navigator.pop(context);
                  },
                  onError: (error){
                    Navigator.pop(context);
                    showErrorDialog(context, error);
                  });
                }, child: const Text("resend"))
              ],
            ),
            const SizedBox(height: 84),
            SizedBox(
              width: double.infinity,
              child: FilledButton(
                onPressed: (enableButton) ? () async {
                  await verifyOTP(
                      widget.email,
                      code.text,
                      onResponse: (){
                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => const SetNewPasswordPage()));
                      },
                      onError: (error){
                        setState(() {
                          isError = true;
                        });
                        showErrorDialog(context, error);
                      });
                } : null,
                child: Text("Set New Password",style: Theme.of(context).textTheme.labelLarge)
              ),
            )
          ],
        ),
      ),
    );
  }
}