import '../data/repository/on_boarding_repository.dart';

class Queue {
  final List<Map<String, String>> _data = [];

  void push(Map<String, String> elem) {
    var count = 0;
    for (var _ in _data){
      count ++;
    }
    _data.insert(count, elem);
  }

  Map<String, String> next(){
    var elem = _data[0];
    for (var i in _data.reversed){
      elem = i;
    }
    _data.remove(elem);
    return elem;
  }

  bool isEmpty(){
    var count = 0;
    for (var _ in _data){
      count ++;
    }
    return count == 0;
  }

  int length(){
    return _data.length;
  }

  void resetQueue(){
    _data.clear();
    for (var elem in getOnBoardingItems()){
      _data.add(elem);
    }
  }
}