import 'package:final_wsr_2023/on_boarding/domain/queue.dart';
import 'package:final_wsr_2023/on_boarding/presentation/widgets/item_on_boarding.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_sign_up.dart';
import 'package:flutter/material.dart';

class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({super.key});

  @override
  State<OnBoardingPage> createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {

  final PageController pageController = PageController(initialPage: 0);
  final Queue queue = Queue();
  Map<String, String> currentElement = {};

  @override
  void initState() {
    super.initState();
    queue.push({
      "title": "Quick Delivery At Your\nDoorstep",
      "text": "Enjoy quick pick-up and delivery to\nyour destination",
      "image": "assets/first.png",
    });
    queue.push({
      "title": "Flexible Payment",
      "text": "Different modes of payment either\nbefore and after delivery without\nstress",
      "image": "assets/second.png",
    });
    queue.push({
      "title": "Real-time Tracking",
      "text": "Track your packages/items from the\ncomfort of your home till final destination",
      "image": "assets/third.png",
    });
    currentElement = queue.next();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            Expanded(
              child: PageView.builder(
                allowImplicitScrolling: false,
                controller: pageController,
                itemCount: queue.length(),
                itemBuilder: (context, index) {
                  return ItemOnBoarding(model: currentElement);
                },
                onPageChanged: (index){
                  setState(() {
                    currentElement = queue.next();
                  });
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22.0),
              child: (queue.isEmpty()) ? getSignUpPanel() : panelButton(),
            ),
            SizedBox(height: (queue.isEmpty()) ? 64 : 99)
          ],
        ),
      ),
    );
  }

  Column getSignUpPanel() {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: FilledButton(
            key: const ValueKey("sign_up_button"),
            style: FilledButton.styleFrom(
              textStyle: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700
              )
            ),
            onPressed: (){
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => const SignUpPage())
              );
            },
            child: const Text("Sign Up")
          ),
        ),
        const SizedBox(height: 20),
        RichText(text: const TextSpan(
          children: [
            TextSpan(text: "Already have an account?", style: TextStyle(color: Color(0xFFA7A7A7))),
            TextSpan(text: "Sign in", style: TextStyle(color: Color(0xFF0560FA), fontWeight: FontWeight.w500)),
          ]
        )),
      ],
    );
  }

  Row panelButton() {
    return Row(
      children: [
        Expanded(
          child: Align(
            alignment: Alignment.centerLeft,
            child: OutlinedButton(
                onPressed: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => const SignUpPage())
                  );
                },
                child: const Text('Skip')
            ),
          ),
        ),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: FilledButton(
                onPressed: () {
                  setState(() {
                    pageController.nextPage(duration: const Duration(milliseconds: 200), curve: Curves.linear);
                  });
                },
                child: const Text('Next')
            ),
          ),
        ),
      ],
    );
  }

}
