import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:final_wsr_2023/common/widgets/dialogs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

Future<Map<String, String>> readAsset(String assetName) async {
  String content = await rootBundle.loadString('assets/data.json');
  return jsonDecode(content);
}

Future<List<List<String>>> readCSV(String assetName) async {
  String content = await rootBundle.loadString('assets/data.csv');
  List<List<String>> data = [];
  for (var line in content.split("\n")){
    data.add(line.split(";"));
  }
  return data;
}

Future<void> checkAndComplete(BuildContext context, Function onGood) async {
  // не использовать
  var connectivityResult = await Connectivity().checkConnectivity();
  if (connectivityResult == ConnectivityResult.none){
    showErrorNetwork(context);
  }else{
    onGood();
  }
}

Future<bool> checkNetworkConnection() async {
  var connectivityResult = await Connectivity().checkConnectivity();
  return connectivityResult != ConnectivityResult.none;
}