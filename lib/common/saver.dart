import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Saver {
  final SharedPreferences sharedPreferences;

  const Saver(this.sharedPreferences);

  static Future<Saver> getInstance() async {
    return Saver(await SharedPreferences.getInstance());
  }

  Future<int> getCounter() async {
    return sharedPreferences.getInt("counter") ?? 0;
  }

  Future<void> setCounter(int newCounter) async {
    await sharedPreferences.setInt("counter", newCounter);
  }

  Future<String?> getHashPassword() async {
    return sharedPreferences.getString("hash_password");
  }

  Future<void> savePassword(String password) async {
    var bytes = utf8.encode(password);
    var shaPassword = sha256.convert(bytes).toString();
    await sharedPreferences.setString("hash_password", shaPassword);
  }
}