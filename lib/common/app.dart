import 'package:final_wsr_2023/auth/presentation/pages/page_sign_up.dart';
import 'package:final_wsr_2023/common/theme.dart';
import 'package:final_wsr_2023/home/presentation/pages/page_home.dart';
import 'package:flutter/material.dart';

import '../auth/data/repository/supabase.dart';


class MyApp extends StatefulWidget {
  MyApp({super.key});
  var isLightTheme = true;

  void changeTheme(BuildContext context, bool isLightTheme) {
    this.isLightTheme = isLightTheme;
    context.findAncestorStateOfType<_MyAppState>()!.onChangeTheme();
  }

  ThemeData getCurrentTheme(){
    return (isLightTheme) ? lightTheme : darkTheme;
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  
  void onChangeTheme(){
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: widget.getCurrentTheme(),
      home: (supabase.auth.currentUser == null) ? const SignUpPage() : const HomePage(),
    );
  }
}