import 'package:flutter/material.dart';

class CustomField extends StatelessWidget{
  final String hint;
  final TextEditingController controller;
  const CustomField({super.key, required this.hint, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Color(0x26000026),
                  blurRadius: 5,
                  offset: Offset(0, 2)
              )
            ]
        ),
        child: SizedBox(
            height: 32,
            child: TextField(
              controller: controller,
              style: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF3A3A3A)
              ),
              decoration: InputDecoration(
                hintText: hint,
                isDense: true,
                contentPadding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                hintStyle: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFFCFCFCF)
                ),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
              ),
            )
        )
    );
  }
}
