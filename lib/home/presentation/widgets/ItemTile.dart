import 'package:flutter/material.dart';

class ItemTile extends StatelessWidget {
  final String title;
  final String? subtitle;
  final String icon;

  const ItemTile(
      {super.key, required this.title, this.subtitle, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Container(
        decoration: const BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              color: Color(0x26000000), blurRadius: 5, offset: Offset(0, 2))
        ]),
        child: ListTile(
          contentPadding: const EdgeInsets.symmetric(horizontal: 12),
          leading: Image.asset(icon),
          title: Text(title, style: Theme.of(context).textTheme.labelMedium!.copyWith(
            fontSize: 16
          ),),
          subtitle: (subtitle != null) ? Text(subtitle!, style: Theme.of(context).textTheme.titleMedium!.copyWith(
            fontWeight: FontWeight.w400, fontSize: 12
          ),) : null,
          trailing: const Icon(
            Icons.arrow_forward_ios_rounded,
            color: Color(0xFF292D32),
            size: 14,
          ),
        ),
      ),
    );
  }
}
