import 'package:final_wsr_2023/home/presentation/pages/page_home.dart';
import 'package:flutter/material.dart';

class Transition extends StatefulWidget {
  final String id;
  const Transition({super.key, required this.id});


  @override
  State<Transition> createState() => _TransitionState();
}

class _TransitionState extends State<Transition> with SingleTickerProviderStateMixin {

  late AnimationController _controller;
  bool isFinish = false;
  
  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _controller.forward().whenComplete(
        () => setState((){
          isFinish = true;
        })
    );
    return Scaffold(
        body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 144),
                (!isFinish) ? RotationTransition(
                    turns: Tween(begin: 1.0, end: 0.0).animate(_controller),
                    child: Image.asset("assets/Component 21.png")
                ) : Image.asset("assets/Good Tick.png"),
                const SizedBox(height: 130,),
                Text(
                    "Your rider is on the way to your destination",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                ),
                const SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                        "Tracking Number ",
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                    ),
                    Text(
                      (widget.id.length <= 21) ? "R-${widget.id}" : "${"R-${widget.id}".substring(0, 18)}...",

                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: Color(0xFF0560FA)),
                    ),
                  ],
                ),
                const SizedBox(height: 141),
                SizedBox(
                  height: 46,
                  width: 342,
                  child: OutlinedButton(
                      style: ButtonStyle(
                        backgroundColor: const MaterialStatePropertyAll<Color>(Color(0xFF0560FA),),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              side: const BorderSide(width: 0),
                              borderRadius: BorderRadius.circular(4),

                            )
                        ),
                      ),
                      onPressed: (){
                        // Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(current_index: 2)));
                      },
                      child: const Text("Track my item", style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w700
                      ),)),
                ),
                const SizedBox(height: 8),
                SizedBox(
                  height: 46,
                  width: 342,
                  child: OutlinedButton(
                      style: ButtonStyle(
                        side: MaterialStateProperty.all(
                            const BorderSide(
                              color: Color(0xFF0560FA),
                              width: 1.0,
                              style: BorderStyle.solid,)),
                        backgroundColor: const MaterialStatePropertyAll<Color>(Colors.white,),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4),

                          ),
                        ),
                      ),
                      onPressed: (){
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => const HomePage()),
                          (_) => false
                        );
                      },
                      child: const Text("Go back to homepage", style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF0560FA),
                          fontWeight: FontWeight.w700
                      ),)),
                )
              ],
            ))

    );
  }
}
