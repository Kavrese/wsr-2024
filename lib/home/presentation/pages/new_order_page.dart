import 'package:final_wsr_2023/home/presentation/pages/send_a_package.dart';
import 'package:flutter/material.dart';

import '../../data/repository/supabase.dart';
import '../widgets/text_field.dart';

class NewOrderPage extends StatefulWidget {
  const NewOrderPage({super.key});

  @override
  State<NewOrderPage> createState() => _NewOrderPageState();
}

class _NewOrderPageState extends State<NewOrderPage> {
  var address_controller = TextEditingController();
  var country_controller = TextEditingController();
  var phone_controller = TextEditingController();
  var others_controller = TextEditingController();
  var items_controller = TextEditingController();
  var weight_controller = TextEditingController();
  var worth_controller = TextEditingController();
  int count = 1;
  List<TextEditingController> adresses = [TextEditingController()];
  List<TextEditingController> countries = [TextEditingController()];
  List<TextEditingController> phones = [TextEditingController()];
  List<TextEditingController> otherses = [TextEditingController()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    color: Color(0x26000026),
                    blurRadius: 5,
                    offset: Offset(0, 2))
              ]),
              child: Stack(children: [
                const Align(
                  alignment: Alignment.bottomCenter,
                  child: Text("Send a package",
                      style: TextStyle(color: Color(0xFFA7A7A7), fontSize: 16, fontWeight: FontWeight.w500)
                  ),
                ),
                Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Image.asset("assets/arrow-square-right.png"),
                    ))
              ]),
            ),
          ),
          const SliverToBoxAdapter(
              child: SizedBox(height: 32)
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset("assets/Frame 60.png"),
                      const SizedBox(
                        width: 8,
                      ),
                      Text(
                        "Origin Details",
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomField(hint: "Address", controller: address_controller),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomField(
                      hint: "State,Country", controller: country_controller),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomField(
                      hint: "Phone number", controller: phone_controller),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomField(hint: "Others", controller: others_controller),
                ],
              ),
            ),
          ),
          SliverList.builder(
            itemCount: count,
            itemBuilder: (_, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: <Widget>[
                    const SizedBox(
                      height: 39,
                    ),
                    Row(
                      children: [
                        Image.asset("assets/Frame 59.png"),
                        const SizedBox(
                          width: 8,
                        ),
                        Text(
                          "Destination Details",
                          style: Theme.of(context).textTheme.labelMedium,
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    CustomField(
                        hint: "Address", controller: adresses[index]),
                    const SizedBox(
                      height: 5,
                    ),
                    CustomField(
                        hint: "State,Country", controller: countries[index]),
                    const SizedBox(
                      height: 5,
                    ),
                    CustomField(
                        hint: "Phone number", controller: phones[index]),
                    const SizedBox(
                      height: 5,
                    ),
                    CustomField(hint: "Others", controller: otherses[index]),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              );
            },
          ),
          const SliverToBoxAdapter(child: SizedBox(height: 17)),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        count++;
                        adresses.add(TextEditingController());
                        phones.add(TextEditingController());
                        countries.add(TextEditingController());
                        otherses.add(TextEditingController());
                      });
                    },
                    child: Image.asset("assets/add-square.png"),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  const Text(
                    "Add destination",
                    style: TextStyle(
                        fontSize: 12,
                        color: Color(0xFFA7A7A7),
                        fontWeight: FontWeight.w400),
                  )
                ],
              ),
          )),
          const SliverToBoxAdapter(child: SizedBox(height: 24)),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  const SizedBox(
                    height: 13,
                  ),
                  Row(
                    children: [
                      Text(
                        "Package Details",
                        style: Theme.of(context).textTheme.labelMedium,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomField(
                      hint: "package items", controller: items_controller),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomField(
                      hint: "Weight of item(kg)",
                      controller: weight_controller),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomField(
                      hint: "Worth of Items", controller: worth_controller),
                  const SizedBox(
                    height: 39,
                  ),
                  Row(
                    children: [
                      Text(
                        "Select delivery type",
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Align(
                          alignment: Alignment.bottomLeft,
                          child: InkWell(
                            onTap: () async {
                              double deliveryCharges = adresses.length * 2500;
                              double instantDelivery = 300.00;
                              double tax =
                                  (deliveryCharges + instantDelivery) * 0.05;
                              double sumPrice =
                                  tax + instantDelivery + deliveryCharges;
                              List<Map<String, String>> destinations = [];
                              for (int i = 0; i < count; i++) {
                                destinations.add({
                                  "address": adresses[i].text,
                                  "country": countries[i].text,
                                  "phone": phones[i].text,
                                  "others": otherses[i].text
                                });
                              }
                              String id = await insertNewOrder(
                                  address_controller.text,
                                  country_controller.text,
                                  phone_controller.text,
                                  others_controller.text,
                                  items_controller.text,
                                  int.parse(weight_controller.text),
                                  int.parse(worth_controller.text),
                                  deliveryCharges,
                                  instantDelivery,
                                  tax,
                                  sumPrice,
                                  destinations
                              );
                               Navigator.push(
                                   context,
                                   MaterialPageRoute(
                                       builder: (context) => Send_package_2_Page(
                                           id: id,
                                           destinations: destinations,
                                           address: address_controller.text,
                                           country: country_controller.text,
                                           phone: phone_controller.text,
                                           others: others_controller.text,
                                           items: items_controller.text,
                                           weight: weight_controller.text,
                                           worth: worth_controller.text,
                                           delivery_charges:
                                               deliveryCharges.toString(),
                                           instantDelivery:
                                               instantDelivery.toString(),
                                           tax: tax.toString(),
                                           sum_price: sumPrice.toString())));
                            },
                            child: Image.asset("assets/Frame 78.png"),
                          )),
                      const SizedBox(
                        width: 24,
                      ),
                      Align(
                          alignment: Alignment.bottomRight,
                          child: InkWell(
                            onTap: () {},
                            child: Image.asset("assets/Frame 82.png"),
                          )),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
