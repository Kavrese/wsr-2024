import 'package:final_wsr_2023/home/presentation/pages/page_transaction.dart';
import 'package:flutter/material.dart';

class Send_package_2_Page extends StatefulWidget {
  final String id;
  final List destinations;
  final String address;
  final String country;
  final String phone;
  final String others;
  final String items;
  final String weight;
  final String worth;
  final String delivery_charges;
  final String instantDelivery;
  final String tax;
  final String sum_price;

  const Send_package_2_Page({super.key, required this.id, required this.destinations, required this.address, required this.country, required this.phone, required this.others, required this.items, required this.weight, required this.worth, required this.delivery_charges, required this.instantDelivery, required this.tax, required this.sum_price});


  @override
  State<Send_package_2_Page> createState() => _Send_package_2_PageState();
}

class _Send_package_2_PageState extends State<Send_package_2_Page> {
  String destinationText = '';


  @override
  void initState() {
    super.initState();
    var elements = widget.destinations.map(
            (e) => e["address"] + ", " + e["country"] + "\n" + e["phone"]
    ).toList();
    var elementsWithIndex = [];
    for (int index = 0; index < elements.length; index++){
      elementsWithIndex.add("${index+1}. ${elements[index]}");
    }
    destinationText = elementsWithIndex.join("\n");
  }
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 108,
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
                decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
                ]),
                child: Stack(children: [
                  const Align(
                    alignment: Alignment.bottomCenter,
                    child: Text("Send a package",
                        style: TextStyle(color: Color(0xFFA7A7A7), fontSize: 16, fontWeight: FontWeight.w500)),
                  ),
                  Align(alignment: Alignment.bottomLeft,
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Image.asset("assets/arrow-square-right.png"),
                      ))
                ]),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 27),
                    Text("Package Information", style:Theme.of(context).textTheme.labelMedium?.copyWith(
                      color: const Color(0xFF0560FA),
                      fontSize: 16
                    ),),
                    const SizedBox(height: 8),
                    Text(
                      "Origin details",
                      style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: const Color(0xFF3A3A3A)
                      )
                    ),
                    const SizedBox(height: 4),
                    Text(
                      "${widget.address}, ${widget.country}\n${widget.phone}",
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFFA7A7A7)
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text("Destination details", style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF3A3A3A)),),
                    Text(destinationText, style: Theme.of(context).textTheme.titleSmall),
                    const SizedBox(height: 8),
                    Text("Other details", style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF3A3A3A)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Package Items",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            widget.items,
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 7),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Weight of items",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            widget.weight,
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 7),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Worth of Items",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            widget.worth,
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 7),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Tracking Number",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            "R-${widget.id}",
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 37),
                    const Divider(
                      color: Colors.black,
                    ),
                    const SizedBox(height: 8,),
                    Text("Charges", style:Theme.of(context).textTheme.titleLarge?.copyWith(color: Color(0xFF0560FA)),),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Delivery Charges",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            widget.delivery_charges,
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 7),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Instant delivery",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            widget.instantDelivery,
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Tax and Service Charges",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            "${widget.tax}",
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 9,),
                    const Divider(
                      color: Colors.black,
                    ),
                    const SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Package total",
                            style: Theme.of(context).textTheme.titleSmall
                        ),
                        Text(
                            widget.sum_price,
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    const SizedBox(height: 46),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: SizedBox(
                              height: 48,
                              child: OutlinedButton(
                                  style: ButtonStyle(
                                    backgroundColor: const MaterialStatePropertyAll<Color>(Colors.white,),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(4),
                                            side: const BorderSide(width: 1, color: Color(0xFF0560FA))
                                        )
                                    ),
                                  ),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text("Edit package", style: TextStyle(
                                      fontSize: 16,
                                      color: Color(0xFF0560FA),
                                      fontWeight: FontWeight.w700
                                  ),))),
                        ),
                        const SizedBox(width: 24),
                        Expanded(
                          child: SizedBox(
                            height: 48,
                            child: FilledButton(
                                style: OutlinedButton.styleFrom(
                                  padding: EdgeInsets.zero,
                                ),
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => Transition(id: widget.id)));
                                },
                                child: const Text("Make payment", style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700
                                ),)),),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}
