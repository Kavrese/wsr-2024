import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:final_wsr_2023/auth/data/repository/supabase.dart';
import 'package:final_wsr_2023/common/utils.dart';
import 'package:final_wsr_2023/common/widgets/dialogs.dart';
import 'package:final_wsr_2023/home/data/repository/supabase.dart';
import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Track extends StatefulWidget {
  const Track({super.key});

  @override
  State<Track> createState() => _TrackState();
}

class _TrackState extends State<Track> {

  YandexMapController? mapController;
  Map<String, dynamic> order = {};
  bool isFinishLoading = false;
  List dates = ["", "", "", ""];
  List<MapObject> mapObjects = [];
  List<Point> mapPoints = [];


  @override
  void initState() {
    super.initState();
    subscribeToUpdateOrder("9b9dd144-b27c-4dae-88a5-9af0e039876b", (newOrder) {
      setState(() {
        isFinishLoading = true;
        order = newOrder;
      });
    });
  }

  Future<void> initMapObjects() async {
    mapPoints = await getPoints().then((value) {
      return value.map((e) => Point(
          latitude: double.parse(e["latitude"]),
          longitude: double.parse(e["longitude"])
      )).toList();
    });
    for(var value in mapPoints){
      mapObjects.add(
          PlacemarkMapObject(
              opacity: 1,
              mapId: MapObjectId("point-${value.latitude}-${value.longitude}"),
              icon: PlacemarkIcon.single(PlacemarkIconStyle(
                scale: 4,
                image: BitmapDescriptor.fromAssetImage("assets/marker_small.png"),
                anchor: const Offset(0.5, 1)
              )),
              point: value
          )
      );
    }
    mapObjects.add(
        PolygonMapObject(
          mapId: const MapObjectId('polygon map object'),
          polygon: Polygon(
            outerRing: LinearRing(
              points: mapPoints,
            ),
            innerRings: const [],
          ),
          strokeColor: const Color(0xFF0560FA),
          strokeWidth: 1.0,
          fillColor: const Color(0xFF0560FA),
        )
    );
    await mapController?.moveCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: mapPoints.first,
          zoom: 12,
        ),
      ),
    );
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (isFinishLoading) ? Column(
        children: [
          SizedBox(
            height: 320,
            child: YandexMap(
              onMapCreated: (controller) async {
                mapController = controller;
                if (mapPoints.isEmpty) {
                    await initMapObjects();
                }
              },
              mapObjects: mapObjects,
            ),
          ),
          const SizedBox(height: 24),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    SizedBox.square(
                      dimension: 14,
                      child: Checkbox(
                        value: order["status"] >= 0,
                        onChanged: (bool? value) {  },
                      ),
                    ),
                    SizedBox(width: 14, child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        width: 1,
                        height: 34,
                        color: const Color(0xFFA7A7A7),
                      ),
                    ),),
                    SizedBox.square(
                      dimension: 14,
                      child: Checkbox(
                        value: order["status"] >= 1,
                        onChanged: (bool? value) {  },
                      ),
                    ),
                    SizedBox(width: 14, child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        width: 1,
                        height: 34,
                        color: const Color(0xFFA7A7A7),
                      ),
                    ),),
                    SizedBox.square(
                      dimension: 14,
                      child: Checkbox(
                        value: order["status"] >= 2,
                        onChanged: (bool? value) {  },
                      ),
                    ),
                    SizedBox(width: 14, child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        width: 1,
                        height: 34,
                        color: const Color(0xFFA7A7A7),
                      ),
                    ),),
                    SizedBox.square(
                      dimension: 14,
                      child: Checkbox(
                        value: order["status"] >= 3,
                        onChanged: (bool? value) {  },
                      ),
                    ),
                  ],
                ),
                const SizedBox(width: 7),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Courier requested", style: TextStyle(fontSize: 14, height: 1), textAlign: TextAlign.start,),
                    const SizedBox(height: 4),
                    Text(dates[0], style: const TextStyle(fontSize: 12, height: 1.5)),
                    const SizedBox(height: 12),
                    const Text("Package ready for delivery", style: TextStyle(fontSize: 14, height: 1)),
                    const SizedBox(height: 4),
                    Text(dates[1], style: const TextStyle(fontSize: 12, height: 1.5)),
                    const SizedBox(height: 12),
                    const Text("Package in transit", style: TextStyle(fontSize: 14, height: 1)),
                    const SizedBox(height: 4),
                    Text(dates[2], style: const TextStyle(fontSize: 12, height: 1.5)),
                    const SizedBox(height: 12),
                    const Text("Package delivered", style: TextStyle(fontSize: 14, height: 1)),
                    const SizedBox(height: 4),
                    Text(dates[3], style: const TextStyle(fontSize: 12, height: 1.5)),
                    const SizedBox(height: 12),
                  ],
                ),
              ],
            ),
          )
        ],
      ) : const Center(
        child: CircularProgressIndicator(),
      )
    );
  }
}