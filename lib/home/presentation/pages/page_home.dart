import 'package:final_wsr_2023/home/presentation/pages/profile.dart';
import 'package:final_wsr_2023/home/presentation/pages/track.dart';
import 'package:final_wsr_2023/home/presentation/pages/wallet.dart';
import 'package:flutter/material.dart';

import 'home.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  int currentPosition = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 60,
        decoration: const BoxDecoration(boxShadow: [
          BoxShadow(
              color: Color(0x0d00000d), blurRadius: 10, offset: Offset(0, -4))
        ]),
        child: BottomNavigationBar(
          currentIndex: currentPosition,
          elevation: 0,
          selectedFontSize: 12,
          unselectedItemColor: const Color(0xFFA7A7A7),
          selectedItemColor: const Color(0xFF0560FA),
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: true,
          items: [
            BottomNavigationBarItem(
                label: "Home",
                icon: Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Image.asset((currentPosition == 0)
                      ? "assets/house-2.png"
                      : "assets/house-1.png"),
                )),
            BottomNavigationBarItem(
                label: "Wallet",
                icon: Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Image.asset((currentPosition == 1)
                      ? "assets/wallet-2.png"
                      : "assets/wallet-1.png"),
                )),
            BottomNavigationBarItem(
                label: "Track",
                icon: Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Image.asset((currentPosition == 2)
                      ? "assets/track-2.png"
                      : "assets/track-1.png"),
                )),
            BottomNavigationBarItem(
                label: "Profile",
                icon: Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Image.asset((currentPosition == 3)
                      ? "assets/profile-2.png"
                      : "assets/profile-1.png"),
                )),
          ],
          onTap: (index) {
            setState(() {
              currentPosition = index;
            });
          },
        ),
      ),
      body: [const Home(), const Wallet(), const Track(), const Profile()][currentPosition],
    );
  }
}
