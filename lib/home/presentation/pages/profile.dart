import 'package:final_wsr_2023/auth/data/repository/supabase.dart';
import 'package:final_wsr_2023/auth/presentation/pages/page_sign_in.dart';
import 'package:final_wsr_2023/common/widgets/dialogs.dart';
import 'package:final_wsr_2023/home/data/repository/supabase.dart';
import 'package:final_wsr_2023/home/presentation/widgets/ItemTile.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  final ImagePicker picker = ImagePicker();

  bool obscureBalance = false;
  double balance = 0;
  String fullname = "";
  String avatarUrl = "";

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      showLoading(context);
      getUser().then((value) async {
        avatarUrl = await getAvatar();
        setState((){
          fullname = value["fullname"];
          balance = value["balance"];
          Navigator.of(context).pop();
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Container(
          height: 108,
          width: double.infinity,
          alignment: Alignment.bottomLeft,
          padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
          decoration: const BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
                color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
          ]),
          child: const Align(
            alignment: Alignment.bottomCenter,
            child: Text("Profile",
                style: TextStyle(color: Color(0xFFA7A7A7), fontSize: 16, fontWeight: FontWeight.w500)),
          ),
        ),
        const SizedBox(height: 39.5),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            children: [
              GestureDetector(
                onTap: () async {
                  final XFile? image = await picker.pickImage(source: ImageSource.gallery);
                  if (image == null){
                    return;
                  }
                  setState(() {
                    showLoading(context);
                  });
                  await uploadAvatar(await image.readAsBytes());
                  setState(() {
                    Navigator.pop(context);
                  });
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: Container(
                    height: 60.0,
                    width: 60.0,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xFFCFCFCF),
                    ),
                    child: Image.network(
                        avatarUrl,
                        fit: BoxFit.cover, width: 50
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 5),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(fullname,
                        style: Theme.of(context)
                            .textTheme
                            .labelMedium!
                            .copyWith(fontSize: 16)),
                    RichText(
                        text: TextSpan(children: [
                          TextSpan(
                              text: "Current balance: ",
                              style: Theme.of(context)
                                  .textTheme
                                  .labelMedium!
                                  .copyWith(
                                  fontSize: 12, fontWeight: FontWeight.normal)),
                          TextSpan(
                              text: (obscureBalance) ? "**********" : "N${balance.toString()}:00",
                              style: Theme.of(context)
                                  .textTheme
                                  .labelSmall!
                                  .copyWith(
                                  fontSize: 12, fontWeight: FontWeight.w500))
                        ]))
                  ],
                ),
              ),
              GestureDetector(
                  onTap: (){
                    setState(() {
                      obscureBalance = !obscureBalance;
                    });
                  },
                  child: Image.asset(
                    "assets/eye-slash.png",
                    alignment: Alignment.centerRight,
                    height: 14,
                  )
              )
            ],
          ),
        ),
        const SizedBox(height: 26.5),
        const ItemTile(title: "Edit Profile", subtitle: "Name, phone no, address, email ...", icon: "assets/edit_profile.png"),
        const SizedBox(height: 12),
        const ItemTile(title: "Statements & Reports", subtitle: "Download transaction details, orders, deliveries", icon: "assets/reports.png"),
        const SizedBox(height: 12),
        const ItemTile(title: "Notification Settings", subtitle: "mute, unmute, set location & tracking setting", icon: "assets/notif.png"),
        const SizedBox(height: 12),
        const ItemTile(title: "Card & Bank account settings", subtitle: "change cards, delete card details", icon: "assets/cards.png"),
        const SizedBox(height: 12),
        const ItemTile(title: "Referrals", subtitle: "check no of friends and earn", icon: "assets/ref.png"),
        const SizedBox(height: 12),
        const ItemTile(title: "About Us", subtitle: "know more about us, terms and conditions", icon: "assets/map.png"),
        const SizedBox(height: 12),
        GestureDetector(
          child: const ItemTile(title: "Log out", icon: "assets/logout.png"),
          onTap: () async {
              await logout();
              Navigator.of(context)
                  .pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (_) => const SignInPage()
                  ),
                  (route) => false
              );
          },
        ),
      ],
    );
  }
}