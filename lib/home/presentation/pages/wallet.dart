import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../common/widgets/dialogs.dart';
import '../../data/repository/supabase.dart';

class Wallet extends StatefulWidget {
  const Wallet({super.key});

  @override
  State<Wallet> createState() => _WalletState();
}

class _WalletState extends State<Wallet> {

  bool obscureBalance = false;
  double balance = 0.0;
  String fullname = "";
  List<Map<String, dynamic>> transactions = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      showLoading(context);
      var value = await getUser();
      transactions = await getTransaction();
      setState((){
        fullname = value["fullname"];
        balance = value["balance"];
        Navigator.of(context).pop();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
          SliverToBoxAdapter(
            child: Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
              ]),
              child: const Align(
                alignment: Alignment.bottomCenter,
                child: Text("Wallet",
                    style: TextStyle(color: Color(0xFFA7A7A7), fontSize: 16, fontWeight: FontWeight.w500)),
              ),
            ),
          ),
          const SliverToBoxAdapter(child: SizedBox(height: 39.5)),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: Container(
                      height: 60.0,
                      width: 60.0,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xFFCFCFCF),
                      ),
                      child: Image.asset("assets/avatar.jpg",
                          fit: BoxFit.cover, width: 50),
                    ),
                  ),
                  const SizedBox(width: 5),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(fullname,
                            style: Theme.of(context)
                                .textTheme
                                .labelMedium!
                                .copyWith(fontSize: 16)),
                        RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text: "Current balance: ",
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelMedium!
                                      .copyWith(
                                      fontSize: 12, fontWeight: FontWeight.normal)),
                              TextSpan(
                                  text: (obscureBalance) ? "**********" : "N${balance.toString()}:00",
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelSmall!
                                      .copyWith(
                                      fontSize: 12, fontWeight: FontWeight.w500))
                            ]))
                      ],
                    ),
                  ),
                  GestureDetector(
                      onTap: (){
                        setState(() {
                          obscureBalance = !obscureBalance;
                        });
                      },
                      child: Image.asset(
                        "assets/eye-slash.png",
                        alignment: Alignment.centerRight,
                        height: 14,
                      )
                  )
                ],
              ),
            ),
          ),
          const SliverToBoxAdapter(child: SizedBox(height: 28)),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: const Color(0xFFCFCFCF),
                    ),
                    child: Column(
                      children: [
                        const SizedBox(height: 10),
                        const Text("Top Up", style: TextStyle(fontSize: 16, color: Color(0xFF3A3A3A), fontWeight: FontWeight.w700)),
                        const SizedBox(height: 12),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(30),
                                  child: Container(
                                    width: 49,
                                    height: 49,
                                    padding: const EdgeInsets.all(14),
                                    color: const Color(0xFF0560FA),
                                    child: Image.asset("assets/Vector-1.png"),
                                  ),
                                ),
                                const SizedBox(height: 4),
                                const Text("Bank", style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 12),)
                              ],
                            ),
                            Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(30),
                                  child: Container(
                                    width: 49,
                                    height: 49,
                                    padding: const EdgeInsets.all(14),
                                    color: const Color(0xFF0560FA),
                                    child: Image.asset("assets/Vector-2.png"),
                                  ),
                                ),
                                const SizedBox(height: 4),
                                const Text("Transfer", style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 12),)
                              ],
                            ),
                            Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(30),
                                  child: Container(
                                    width: 49,
                                    height: 49,
                                    padding: const EdgeInsets.all(14),
                                    color: const Color(0xFF0560FA),
                                    child: Image.asset("assets/Vector-3.png"),
                                  ),
                                ),
                                const SizedBox(height: 4),
                                const Text("Card", style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 12),)
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(height: 10),
                      ],
                    ),
                  ),
                  const SizedBox(height: 41),
                  Text("Transaction History", style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      fontSize: 20
                  )),

                ],
              ),
            ),
          ),
          const SliverToBoxAdapter(child: SizedBox(height: 24)),
          SliverList.separated(
            itemCount: transactions.length,
            itemBuilder: (_, index) {
              var transaction = transactions[index];
              String sum = transaction["sum"].replaceFirst("\$", "N");
              String dateTransaction = transaction["created_at"];
              DateTime date = DateFormat('yyyy-dd-MM').parse(dateTransaction);
              String textDate = DateFormat("MMMM d, yyyy").format(date);
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Container(
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0x26000000),
                        blurRadius: 5,
                        offset: Offset(0, 2)
                      )
                    ]
                  ),
                  padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                  child: Row(
                    children: [
                      Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(sum, style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: (sum[0] == "-") ? const Color(0xFFED3A3A) : const Color(0xFF35B369)
                              )),
                              const SizedBox(height: 4),
                              Text(transaction["title"], style: const TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500
                              ))
                            ],
                          )
                      ),
                      Text(textDate)
                    ],
                  ),
                ),
              );
            }, separatorBuilder: (BuildContext context, int index) {
              return const SizedBox(height: 12);
          },
          )
        ],
    );
  }
}