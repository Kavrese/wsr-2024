import 'dart:typed_data';
import 'package:http/http.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


final supabase = Supabase.instance.client;

Future<String> insertNewOrder(
  String address,
  String country,
  String phone,
  String others,
  String items,
  int weight,
  int worth,
  double deliveryCharges,
  double instantDelivery,
  double tax,
  double sum,
  List<Map<String, String>> destinations
) async {
  String idOrder = await supabase
      .from("orders")
      .insert({
        "id_user": supabase.auth.currentUser!.id,
        "address": address,
        "country": country,
        "phone": phone,
        "others": others,
        "package_items": items,
        "weight_items": weight,
        "worth_items": worth,
        "delivery_charges": deliveryCharges,
        "instant_delivery": instantDelivery,
        "tax_and_service_charges": tax,
        "sum_price": sum
      })
      .select()
      .single()
      .then((value) => value["id"]);
    
  for (var i in destinations){
    i["id_order"] = idOrder;
    await supabase
        .from("destinations_details")
        .insert(i);
  }
  return idOrder;
}

Future<Map<String, dynamic>> getUser() async {
  return await supabase
      .from("profiles")
      .select()
      .eq("id_user", supabase.auth.currentUser!.id)
      .single();
}

Future<List<Map<String, dynamic>>> getTransaction() async {
  return await supabase
      .from("transactions")
      .select()
      .eq("id_user", supabase.auth.currentUser!.id);
}

Future<Map<String, dynamic>> getOrder() async {
  return await supabase
      .from("orders")
      .select()
      .order("created_at", ascending: true)
      .then((value) => value.last);
}

void subscribeToUpdateOrder(String idOrder, Function(Map<String, dynamic>) callback) {
  supabase
      .channel("orders-status-changes")
      .onPostgresChanges(
        event: PostgresChangeEvent.update,
        schema: "public",
        table: "orders",
        filter: PostgresChangeFilter(
            type: PostgresChangeFilterType.eq,
            column: "id",
            value: idOrder
        ),
        callback: (payload) {
          callback(payload.newRecord);
        }
      )
    .subscribe();
}

Future<String> uploadAvatar(Uint8List bytes) async {
  return await supabase.storage.from('files').uploadBinary(
    '${supabase.auth.currentUser!.id}.png',
    bytes,
    fileOptions: const FileOptions(cacheControl: '3600', upsert: false),
  );
}

Future<String> getAvatar() async {
  var url = 'https://cfjtdxsfxtqxnymxrowo.supabase.co/storage/v1/object/public/files/${supabase.auth.currentUser!.id}.png';
  final response = await get(
      Uri.parse(url)
  );

  if (response.statusCode == 200){
    return url;
  }else{
    return "https://cfjtdxsfxtqxnymxrowo.supabase.co/storage/v1/object/public/files/default.png";
  }
}